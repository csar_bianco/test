$(document).ready(function(){

    $("#resultadomessage").hide();
    $("#matricesdisponibles").val("NULL");
    $("#matricesdisponibles").on("change",function(){
        if($("#matricesdisponibles").val()=="NULL"){
            $("#resultadomessage").hide();
            $("#resultadomessage").show();
            $("#resultadomessage").removeClass("alert-success");
            $("#resultadomessage").addClass("alert-danger");
            $("#resultadomessage").text("Debe seleccionar una matriz correcta.");
        }else{
            $("#resultadomessage").hide();
            $("#resultadomessage").removeClass("alert-danger");
            $("#resultadomessage").removeClass("alert-success");
        }
    });

    $("#btnSeleccionar").on("click",function(){
        if($("#matricesdisponibles").val()!="NULL"){
            $("#resultadomessage").hide();
            $("#resultadomessage").show();
            $("#resultadomessage").removeClass("alert-danger");
            $("#resultadomessage").removeClass("alert-success");

            var url = window.location.href;
            $.ajax({
                url: (url + "consultar"),
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:"post",
                data:{"matriz":$("#matricesdisponibles").val()},
                beforeSend:function(respuesta){},
                success:function(respuesta){
                    if(respuesta.code=="200"){
                        $("#resultadomessage").hide();
                        $("#resultadomessage").show();
                        $("#resultadomessage").removeClass("alert-danger");
                        $("#resultadomessage").addClass("alert-success");
                        $("#resultadomessage").empty();
                        $("#resultadomessage").append(
                            "<ul class='nav flex-column'>" + 
                                '<li class="nav-item">Horizontalmente ha encontrado (' + respuesta.rectas[0] + ') coincidencia(s) </li>' + 
                                '<li class="nav-item">Verticalmente ha encontrado (' + respuesta.rectas[1] + ') coincidencia(s) </li>' + 
                                '<li class="nav-item">Diagonale se ha encontrado (' + respuesta.diagonal + ') coincidencia(s) </li>' + 
                            '</ul>' +
                            "<ul class='nav flex-column'>" + 
                                '<li class="nav-item">Coincidencias totales (' + (respuesta.rectas[0] + respuesta.rectas[1] + respuesta.diagonal) + ') </li>' + 
                            "</ul>");            
                    }
                },
                error:function(respuesta){
                    if(respuesta.responseJSON.code==""){}
                }
            });
        }else{
            $("#resultadomessage").hide();
            $("#resultadomessage").show();
            $("#resultadomessage").removeClass("alert-success");
            $("#resultadomessage").addClass("alert-danger");
            $("#resultadomessage").text("Debe seleccionar una matriz correcta.");
        }
    });
});