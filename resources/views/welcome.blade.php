<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TEST/COR</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Fonts Awesome -->
		<link rel="stylesheet" href="{{asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/general.css')}}">
    </head>
    <body>
        <div class="box flex-box">
            <div class="box-header flex-box-header">
                <span>
                    SELECCIONE LA OPCIÓN DE SU AGRADO
                </span>
            </div>
            <div class="box-body">
                <form class="form-horizontal">
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label clearfix">Opciones. </label>
                            <div class="input-group">
                                <span class="input-group-addon flex-input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                                <select id="matricesdisponibles" name="matricesdisponibles" class="form-control">
                                    <option value="NULL">Seleccione la opción de su agrado</option>
                                    <option value="{{base64_encode('0')}}">3X3</option>
                                    <option value="{{base64_encode('1')}}">1X10</option>
                                    <option value="{{base64_encode('2')}}">5x5</option>
                                    <option value="{{base64_encode('3')}}">7x2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="control-label clearfix">Resultado: </label>
                        </div>
                        <div class="col-xs-12 flex-center">
                            <span id="resultadomessage" class="alert"></span>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12 flex-center">
                            <button type="button" id="btnSeleccionar" class="btn btn-primary btn-flat ">Seleccionar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/home.js')}}"></script>
    </body>
</html>
