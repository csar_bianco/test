<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cor extends Model
{
    public static function verificarDiagonal($inputs){
        $diagonal = "";
        $countdiagonal = 0;
        $diagonales_n = [];
        $diagonales_i = [];
        for ($i=0; $i < count($inputs); $i++) {
            for ($j=0,$k=1,$m=1; $j < count($inputs[$i]); $j++,$m++) {
                if($i==0 || $j==0){
                    array_push($diagonales_n,$inputs[$i][$j]);
                }

                if($i==$j && $i!=0 && $j!=0){
                    $diagonales_n[0].=$inputs[$i][$j];
                }

                if($i<$j && $i!=0 && $j!=0){
                    $d = $k++;//diagonal
                    $diagonales_n[$d].=$inputs[$i][$j];//diagonal arriba
                };

                if($i>$j && $i!=0 && $j!=0){//diagonal abajo
                    $d = count($diagonales_n) - $m;//diagonal
                    $diagonales_n[$d].=$inputs[$i][$j];
                }

                if(isset($diagonales_i[$i+$j])){//diagonales secundaria
                    $diagonales_i[$i+$j] = $inputs[$i][$j].$diagonales_i[$i+$j];
                }else{
                    array_push($diagonales_i,$inputs[$i][$j]);
                }
            }
        }
        
        for ($i=0; $i < count($diagonales_n); $i++) {
            if(mb_substr_count($diagonales_n[$i], "OIE") > 0){//DIAGONAL =>
                $countdiagonal = mb_substr_count($diagonales_n[$i], "OIE");
            }

            if(mb_substr_count(strrev($diagonales_n[$i]), "OIE") > 0){//DIAGONAL =>
                $countdiagonal += mb_substr_count(strrev($diagonales_n[$i]), "OIE");
            }
        }

        for ($i=0; $i < count($diagonales_i); $i++) {
            if(mb_substr_count($diagonales_i[$i], "OIE") > 0){//DIAGONAL <=
                $countdiagonal = mb_substr_count($diagonales_i[$i], "OIE");
            }

            if(mb_substr_count(strrev($diagonales_i[$i]), "OIE") > 0){//DIAGONAL <=
                $countdiagonal += mb_substr_count(strrev($diagonales_i[$i]), "OIE");
            }
        }

        return $countdiagonal;   
    }

    public static function verificarRectas($inputs){
        $counthorizontal = 0;
        $countvertical = 0;

        $arrayv = [];
        $arrayh = [];
        for($i = 0; $i < count($inputs); $i++){
            array_push($arrayh,"");
            for($j = 0; $j < count($inputs[$i]); $j++){
                $arrayh[$i].=$inputs[$i][$j];
                if($i == 0){
                    array_push($arrayv,$inputs[$i][$j]);
                }else{
                    $arrayv[$j] .= $inputs[$i][$j];
                }
            }
        }

        for($i = 0; $i < count($arrayh); $i++){
            if(mb_substr_count($arrayh[$i], "OIE") > 0){//HORIZONTALMENTE =>
                $counthorizontal = mb_substr_count($arrayh[$i], "OIE");
            }

            if(mb_substr_count(strrev($arrayh[$i]), "OIE") > 0){//HORIZONTALMENTE <=
                $counthorizontal += mb_substr_count(strrev($arrayh[$i]), "OIE");
            }
        }

        for($i = 0; $i < count($arrayv); $i++){
            if(mb_substr_count($arrayv[$i], "OIE") > 0){//VERTICALMENTE =>
                $countvertical = mb_substr_count($arrayv[$i], "OIE");
            }

            if(mb_substr_count(strrev($arrayv[$i]), "OIE") > 0){//VERTICALMENTE <=
                $countvertical += mb_substr_count(strrev($arrayv[$i]), "OIE");
            }
        }

        return [$counthorizontal,$countvertical];
    }
}
