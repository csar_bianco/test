<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\Cor;

class CorController extends Controller
{
    public function consultar(Request $request){
        $matriz = base64_decode($request->input("matriz"));
        $rectas = Cor::verificarRectas(Config::get("matrices.".$matriz));
        $diagonal = Cor::verificarDiagonal(Config::get("matrices.".$matriz));
        return response()->json([
            "code" => 200,
            "rectas" => $rectas,
            "diagonal" => $diagonal 
        ],200);
    }
}
